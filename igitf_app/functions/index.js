const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

exports.notifyNewMessage = functions.firestore.document('wishlist/{wishlistId}').onCreate(async (snapshot, context) => {
  if (snapshot.empty) {
    console.log('No Devices');
    return;
  }
  const deviceIdTokens = await admin
    .firestore()
    .collection('deviceTokens')
    .get();

  const tokens = deviceIdTokens.docs.map(snap => snap.data().device_token);

  console.log('Tokens: ', tokens.length);

  const payload = {
    notification: {
      title: 'New Message to ' + tokens.length,
      body: tokens[0],
      sound: 'default',
      clickAction: 'FLUTTER_NOTIFICATION_CLICK',
    }
  }

  return admin.messaging().sendToDevice(tokens, payload);

  // try {
  //   const response = await admin.messaging().sendToDevice(tokens, payload);
  //   console.log('Notification sent successfully:', response);
  //   // return fcm.sendToDevice(tokens, payload);
  // } catch (err) {
  //   console.log(err);
  // }
});
