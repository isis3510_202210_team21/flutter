import 'package:flutter/material.dart';

ThemeData theme() {
  return ThemeData(
      scaffoldBackgroundColor: Colors.white,
      fontFamily: 'Poppins',
      textTheme: textTheme());
}

TextTheme textTheme() {
  return TextTheme(
    headline1: TextStyle(
      color: Colors.black,
      fontSize: 10.0,
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
    ),
    bodyText1: TextStyle(
      color: Colors.black,
      fontSize: 16.0,
      fontFamily: 'Roboto',
      fontWeight: FontWeight.normal,
    ),
  );
}
