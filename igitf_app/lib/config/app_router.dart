import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:igift/bloc/app/app_bloc.dart';
import 'package:igift/models/product_model.dart';
import 'package:igift/screens/home_screen.dart';
import 'package:igift/screens/map_screen.dart';
import 'package:igift/screens/product_detail_screen.dart';
import 'package:igift/screens/product_list.dart';
import 'package:igift/screens/profile_screen.dart';
import 'package:igift/screens/login_screen.dart';
import 'package:igift/screens/wishlist_screen.dart';
import 'package:igift/screens/signup_screen.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    print('This is route: ${settings.name}');
    switch (settings.name) {
      case '/':
        return HomeScreen.route();
      case HomeScreen.routeName:
        return HomeScreen.route();
      case ProductList.routeName:
        return ProductList.route();
      case WishListScreen.routeName:
        return WishListScreen.route();
      case ProductDetailScreen.routeName:
        return ProductDetailScreen.route(
            product: settings.arguments as ProductModel);
      case LoginScreen.routeName:
        return LoginScreen.route();
      case SignUpScreen.routeName:
        return SignUpScreen.route();
      case ProfileScreen.routeName:
        return ProfileScreen.route();

      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
      settings: RouteSettings(name: '/error'),
      builder: (context) => Scaffold(
        body: Center(
          child: AppBar(title: Text('Error')),
        ),
      ),
    );
  }
}

List<Page> onGenerateAppViewPages(
  AppStatus state,
  List<Page<dynamic>> pages,
) {
  switch (state) {
    case AppStatus.authenticated:
      return [HomeScreen.page()];
    case AppStatus.unauthenticated:
      return [LoginScreen.page()];
  }
}
