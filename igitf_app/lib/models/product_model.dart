import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

part 'product_model.g.dart';

@HiveType(typeId: 0)
class ProductModel extends Equatable {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final String imageUrl;
  @HiveField(2)
  final String category;
  @HiveField(3)
  final String subcategory;
  @HiveField(4)
  final double currentPrice;
  @HiveField(5)
  final String currency;
  @HiveField(6)
  final double likesCount;
  @HiveField(7)
  final double rawPrice;

  const ProductModel({
    required this.name,
    required this.imageUrl,
    required this.category,
    required this.subcategory,
    required this.currentPrice,
    required this.currency,
    required this.likesCount,
    required this.rawPrice,
  });

  @override
  List<Object?> get props => [
        name,
        imageUrl,
        category,
        subcategory,
        currentPrice,
        currency,
        likesCount,
        rawPrice,
      ];

  static ProductModel fromSnapshot(DocumentSnapshot snap) {
    ProductModel product = ProductModel(
      name: snap['name'],
      imageUrl: snap['image_url'],
      category: snap['category'],
      subcategory: snap['subcategory'],
      currentPrice: snap['current_price'],
      currency: snap['currency'],
      likesCount: snap['likes_count'],
      rawPrice: snap['raw_price'],
    );
    return product;
  }
}
