import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class AdsModel extends Equatable {
  final String name;
  final String imageUrl;
  final bool sale;
  final String category;
  final int numClicks;

  const AdsModel({
    required this.name,
    required this.imageUrl,
    required this.sale,
    required this.category,
    required this.numClicks,
  });

  @override
  List<Object?> get props => [
        name,
        imageUrl,
        sale,
        category,
        numClicks,
      ];

  static AdsModel fromSnapshot(DocumentSnapshot snap) {
    AdsModel ads = AdsModel(
      name: snap['name'],
      imageUrl: snap['imageUrl'],
      sale: snap['sale'],
      category: snap['category'],
      numClicks: snap['numClicks'],
    );
    return ads;
  }
}
