import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String id;
  final String? email;
  final String? photo;
  final String? name;

  const User({
    required this.id,
    this.email,
    this.photo,
    this.name,
  });

  static const empty = User(id: '');

  bool get isEmpty => User.empty == this;
  // ignore: unrelated_type_equality_checks
  bool get isNotEmpty => this != User.empty;

  @override
  List<Object?> get props =>[id, email, photo, name];
}
