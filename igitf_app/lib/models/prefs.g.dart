// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prefs.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PrefsUserModelAdapter extends TypeAdapter<PrefsUserModel> {
  @override
  final int typeId = 1;

  @override
  PrefsUserModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PrefsUserModel(
      image_url: fields[2] as String,
      name: fields[0] as String,
      email: fields[1] as String,
      preferences: (fields[3] as Map).cast<String, bool>(),
    );
  }

  @override
  void write(BinaryWriter writer, PrefsUserModel obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.email)
      ..writeByte(2)
      ..write(obj.image_url)
      ..writeByte(3)
      ..write(obj.preferences);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PrefsUserModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
