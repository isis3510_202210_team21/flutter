import 'package:equatable/equatable.dart';
import '/models/product_model.dart';

class WishlistModel extends Equatable {
  final List<ProductModel> products;

  const WishlistModel({this.products = const <ProductModel>[]});

  @override
  List<Object?> get props => [products];

  Map<String, Object> toDocument() {
    return {
      'products': products.map((product) => product.toString()).toList(),
    };
  }
}
