import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:igift/models/product_model.dart';

part 'prefs.g.dart';

@HiveType(typeId: 1)
class PrefsUserModel extends Equatable {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final String email;
  @HiveField(2)
  final String image_url;
  @HiveField(3)
  final Map<String, dynamic> preferences;

  const PrefsUserModel({
    required this.image_url,
    required this.name,
    required this.email,
    required this.preferences,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        name,
        email,
        preferences,
      ];

  static PrefsUserModel fromSnapshot(DocumentSnapshot snap) {
    PrefsUserModel prefsUser = PrefsUserModel(
      name: snap['name'],
      image_url: snap['image_url'],
      email: snap['email'],
      preferences: snap['preferences'],
    );
    return prefsUser;
  }
}
