import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:igift/models/prefs.dart';
import 'package:igift/repositories/preferences/prefs_repository.dart';

part 'preferences_event.dart';
part 'preferences_state.dart';

class PreferencesBloc extends Bloc<PreferencesEvent, PreferencesState> {
  final PrefsRepository _prefsRepository;
  StreamSubscription? _prefsSubscription;

  PreferencesBloc({required PrefsRepository prefRepository})
      : _prefsRepository = prefRepository,
        super(PreferencesLoading()) {
    on<LoadPreferences>(_onLoadPreferences);
    on<UpdatePreferences>(_onUpdatePreferences);
    // on<LoadMoreProducts>(_onLoadMoreProducts);
  }

  void _onLoadPreferences(
    LoadPreferences event,
    Emitter<PreferencesState> emit,
  ) {
    _prefsSubscription?.cancel();
    _prefsSubscription = _prefsRepository
        .getUserPreferences(userEmail: 'martin@gmail.com')
        .listen(
      (prefs) {
        add(
          UpdatePreferences(prefs),
        );
      },
    );
  }

  void _onUpdatePreferences(
    UpdatePreferences event,
    Emitter<PreferencesState> emit,
  ) {
    emit(
      PreferencesLoaded(prefs: event.prefs),
    );
  }
}
