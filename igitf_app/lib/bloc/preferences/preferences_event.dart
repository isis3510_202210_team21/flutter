part of 'preferences_bloc.dart';

abstract class PreferencesEvent extends Equatable {
  const PreferencesEvent();

  @override
  List<Object> get props => [];
}

class LoadPreferences extends PreferencesEvent {}

class UpdatePreferences extends PreferencesEvent {
  final List<PrefsUserModel> prefs;

  const UpdatePreferences(this.prefs);

  @override
  List<Object> get props => [prefs];
}
