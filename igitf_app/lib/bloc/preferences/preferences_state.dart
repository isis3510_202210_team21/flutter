part of 'preferences_bloc.dart';

abstract class PreferencesState extends Equatable {
  const PreferencesState();

  @override
  List<Object> get props => [];
}

class PreferencesLoading extends PreferencesState {}

class PreferencesLoaded extends PreferencesState {
  final List<PrefsUserModel> prefs;

  const PreferencesLoaded({this.prefs = const <PrefsUserModel>[]});

  @override
  List<Object> get props => [prefs];
}
