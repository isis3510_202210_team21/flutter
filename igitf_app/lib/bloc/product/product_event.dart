part of 'product_bloc.dart';

abstract class ProductEvent extends Equatable {
  const ProductEvent();

  @override
  List<Object> get props => [];
}

class LoadProducts extends ProductEvent {}

class ChangeCategoryOfProducts extends ProductEvent {
  final String category;

  const ChangeCategoryOfProducts({
    this.category = '',
  }) : super();

  @override
  List<Object> get props => [category];
}

class LoadMoreProducts extends ProductEvent {
  final List<ProductModel> products;
  final String category;

  const LoadMoreProducts({
    this.products = const <ProductModel>[],
    this.category = '',
  }) : super();

  @override
  List<Object> get props => [products, category];
}

class UpdateProducts extends ProductEvent {
  final List<ProductModel> products;
  final String category;

  const UpdateProducts(this.products, this.category);

  @override
  List<Object> get props => [products, category];
}
