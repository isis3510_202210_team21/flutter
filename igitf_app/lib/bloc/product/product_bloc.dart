import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:igift/models/product_model.dart';
import 'package:igift/repositories/product/product_repository.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final ProductRepository _productRepository;
  StreamSubscription? _productSubscription;

  ProductBloc({required ProductRepository productRepository})
      : _productRepository = productRepository,
        super(ProductLoading()) {
    on<ChangeCategoryOfProducts>(_onLoadProducts);
    on<UpdateProducts>(_onUpdateProducts);
    on<LoadMoreProducts>(_onLoadMoreProducts);
  }

  void _onLoadProducts(
    ChangeCategoryOfProducts event,
    Emitter<ProductState> emit,
  ) {
    _productSubscription?.cancel();
    _productSubscription = _productRepository
        .getProductsByCategory(category: event.category)
        .listen((products) => {
              add(
                UpdateProducts(products, event.category),
              ),
            });
  }

  void _onLoadMoreProducts(
    LoadMoreProducts event,
    Emitter<ProductState> emit,
  ) {
    if (this.state is ProductLoaded) {
      _productSubscription?.cancel();
      var actualProducts = event.products;
      emit(ProductLoading());
      _productSubscription = _productRepository
          .getProductsByCategory(category: event.category)
          .listen((products) => {
                print('Likes ${products[0].likesCount}'),
                actualProducts.add(products[0]),
                print('Actual products ${actualProducts.length}'),
              });
      emit(
        ProductLoaded(products: actualProducts, category: event.category),
      );
    }
  }

  void _onUpdateProducts(
    UpdateProducts event,
    Emitter<ProductState> emit,
  ) {
    emit(ProductLoaded(products: event.products, category: event.category));
  }
}
