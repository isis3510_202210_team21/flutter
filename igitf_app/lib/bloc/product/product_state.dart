part of 'product_bloc.dart';

abstract class ProductState extends Equatable {
  const ProductState();

  @override
  List<Object?> get props => [];
}

class ProductLoading extends ProductState {}

class ProductLoaded extends ProductState {
  final List<ProductModel> products;
  final String category;

  const ProductLoaded(
      {this.products = const <ProductModel>[], this.category = ''});

  @override
  List<Object> get props => [products, category];
}
