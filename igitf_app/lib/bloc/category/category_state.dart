part of 'category_bloc.dart';

@immutable
abstract class CategoryState extends Equatable {
  const CategoryState();

  @override
  List<Object> get props => [];
}

class CategoryLoading extends CategoryState {}

class CategoryLoaded extends CategoryState {
  final List<CategoryModel> categories;
  final String category;

  CategoryLoaded(
      {this.categories = const <CategoryModel>[], this.category = ''});

  @override
  List<Object> get props => [categories, category];
}
