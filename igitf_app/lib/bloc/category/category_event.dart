part of 'category_bloc.dart';

@immutable
abstract class CategoryEvent extends Equatable {
  const CategoryEvent();

  @override
  List<Object> get props => [];
}

class LoadCategories extends CategoryEvent {}

class UpdateCategories extends CategoryEvent {
  final List<CategoryModel> categories;

  UpdateCategories(this.categories);

  @override
  List<Object> get props => [categories];
}

class FilterCategory extends CategoryEvent {
  final String category;

  FilterCategory(this.category);

  @override
  List<Object> get props => [category];
}
