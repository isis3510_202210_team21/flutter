part of 'ads_bloc.dart';

abstract class AdsEvent extends Equatable {
  const AdsEvent();

  @override
  List<Object> get props => [];
}

class LoadAds extends AdsEvent {}

class UpdateAds extends AdsEvent {
  final List<AdsModel> ads;

  UpdateAds(this.ads);

  @override
  List<Object> get props => [ads];
}

class AddClickAd extends AdsEvent {
  final AdsModel ad;

  AddClickAd({required this.ad});

  @override
  List<Object> get props => [ad];
}
