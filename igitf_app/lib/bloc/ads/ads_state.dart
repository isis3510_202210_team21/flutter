part of 'ads_bloc.dart';

abstract class AdsState extends Equatable {
  const AdsState();

  @override
  List<Object> get props => [];
}

class AdsLoading extends AdsState {}

class AdsLoaded extends AdsState {
  final List<AdsModel> ads;
  final List<AdsModel> randomAd;
  final String ad;

  AdsLoaded(
      {this.ads = const <AdsModel>[],
      this.ad = '',
      this.randomAd = const <AdsModel>[]});

  @override
  List<Object> get props => [ads, ad, randomAd];
}
