import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:igift/repositories/ads/ads_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:igift/models/ads_model.dart';

part 'ads_event.dart';
part 'ads_state.dart';

class AdsBloc extends Bloc<AdsEvent, AdsState> {
  final AdsRepository _adsRepository;
  StreamSubscription? _adsSubscription;

  AdsBloc({required AdsRepository adsRepository})
      : _adsRepository = adsRepository,
        super(AdsLoading()) {
    on<LoadAds>(_onLoadAd);
    on<UpdateAds>(_onUpdateAds);
    on<AddClickAd>(_onAddClickAd);
  }

  void _onLoadAd(
    LoadAds event,
    Emitter<AdsState> emit,
  ) {
    _adsSubscription?.cancel();

    _adsSubscription = _adsRepository.getAllAds().listen(
      (ads) {
        final _random = new Random();

        // generate a random index based on the list length
        // and use it to retrieve the element
        AdsModel element = ads[_random.nextInt(ads.length)];
        List<AdsModel> adRandomList = [element];
        add(
          UpdateAds(adRandomList),
        );
      },
    );
  }

  void _onUpdateAds(
    UpdateAds event,
    Emitter<AdsState> emit,
  ) {
    print('Entra ads: ${event.ads}');
    emit(
      AdsLoaded(ads: event.ads),
    );
  }

  void _onAddClickAd(AddClickAd event, Emitter<AdsState> emit) {
    print('LAST: ${_adsRepository.updateClickAd(event.ad).toList()}');
  }
}
