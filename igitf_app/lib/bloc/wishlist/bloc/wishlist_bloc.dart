import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:igift/models/product_model.dart';
import 'package:igift/models/wishlist_model.dart';
import 'package:igift/repositories/local_storage/local_storage_repository.dart';
import 'package:igift/repositories/wishlist/wishlist_repository.dart';

part 'wishlist_event.dart';
part 'wishlist_state.dart';

class WishlistBloc extends Bloc<WishlistEvent, WishlistState> {
  final LocalStorageRepository _localStorageRepository;
  final WishListRepository _wishListRepository;
  StreamSubscription? _wishListSubscription;

  WishlistBloc(
      {required LocalStorageRepository localStorageRepository,
      required WishListRepository wishListRepository})
      : _localStorageRepository = localStorageRepository,
        _wishListRepository = wishListRepository,
        super(WishlistLoading()) {
    on<StartWishlist>(_onStartWishlist);
    on<AddProductToWishlist>(_onAddProductToWishlist);
    on<RemoveProductFromWishlist>(_onRemoveProductFromWishlist);
  }

  void _onStartWishlist(
    StartWishlist event,
    Emitter<WishlistState> emit,
  ) async {
    emit(WishlistLoading());
    try {
      Box box = await _localStorageRepository.openBox();
      List<ProductModel> products = _localStorageRepository.getWishlist(box);
      await Future<void>.delayed(const Duration(seconds: 1));

      emit(
        WishlistLoaded(
          wishlist: WishlistModel(products: products),
        ),
      );
    } catch (_) {
      emit(WishlistError());
    }
  }

  void _onAddProductToWishlist(
    AddProductToWishlist event,
    Emitter<WishlistState> emit,
  ) async {
    if (this.state is WishlistLoaded) {
      try {
        await _wishListSubscription?.cancel();
        Box box = await _localStorageRepository.openBox();
        _localStorageRepository.addProductToWishlist(box, event.product);
        await _wishListRepository
            .addWishlist((this.state as WishlistLoaded).wishlist);
        emit(
          WishlistLoaded(
            wishlist: WishlistModel(
              products:
                  List.from((this.state as WishlistLoaded).wishlist.products)
                    ..add(event.product),
            ),
          ),
        );
      } on Exception {
        emit(WishlistError());
      }
    }
  }

  void _onRemoveProductFromWishlist(
    RemoveProductFromWishlist event,
    Emitter<WishlistState> emit,
  ) async {
    if (this.state is WishlistLoaded) {
      try {
        Box box = await _localStorageRepository.openBox();
        _localStorageRepository.removeProductFromWishlist(box, event.product);

        emit(
          WishlistLoaded(
            wishlist: WishlistModel(
              products:
                  List.from((this.state as WishlistLoaded).wishlist.products)
                    ..remove(event.product),
            ),
          ),
        );
      } on Exception {
        emit(WishlistError());
      }
    }
  }
}
