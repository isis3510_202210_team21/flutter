import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:igift/bloc/category/category_bloc.dart';
import 'package:igift/bloc/preferences/preferences_bloc.dart';
import 'package:igift/bloc/product/product_bloc.dart';
import 'package:igift/bloc/wishlist/bloc/wishlist_bloc.dart';
import 'package:igift/config/app_router.dart';
import 'package:igift/models/prefs.dart';
import 'package:igift/repositories/category/category_repository.dart';
import 'package:igift/repositories/local_storage/local_storage_repository.dart';
import 'package:igift/repositories/preferences/prefs_repository.dart';
import 'package:igift/repositories/product/product_repository.dart';
import 'package:igift/repositories/wishlist/wishlist_repository.dart';
import 'package:igift/screens/home_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

//D
import 'package:igift/firebase_options.dart';
import 'package:igift/screens/login_screen.dart';
import '/bloc_observer.dart';
import 'bloc/app/app_bloc.dart';
import 'config/app_router.dart';
import 'models/product_model.dart';
import 'repositories/auth_repository.dart';
import 'package:igift/bloc/ads/ads_bloc.dart';
import './repositories/ads/ads_repository.dart';
//import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'firebase_options.dart';
import 'package:igift/providers/location_provider.dart';
import 'package:igift/screens/map_screen.dart';
import 'package:provider/provider.dart';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  description:
      'This channel is used for important notifications.', // description
  importance: Importance.high,
  playSound: true,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('A bg message just showed up :  ${message.messageId}');
}

Future<void> main() {
  return BlocOverrides.runZoned(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      await Firebase.initializeApp();
      FirebaseMessaging.onBackgroundMessage(
          _firebaseMessagingBackgroundHandler);

      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);

      await FirebaseMessaging.instance
          .setForegroundNotificationPresentationOptions(
        alert: true,
        badge: true,
        sound: true,
      );

      FirebaseMessaging.instance
          .getToken()
          .then((value) => print('Token: $value'));

      Hive.initFlutter();
      Hive.registerAdapter(ProductModelAdapter());
      Hive.registerAdapter(PrefsUserModelAdapter());
      final AuthRepository authRepository = AuthRepository();
      runApp(MyApp(authRepository: authRepository));
    },
    blocObserver: AppBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  final AuthRepository _authRepository;
  const MyApp({
    Key? key,
    required AuthRepository authRepository,
  })  : _authRepository = authRepository,
        super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => LocationProvider(),
          child: GoogleMapPage(),
        ),
        RepositoryProvider.value(
          value: _authRepository,
          child: BlocProvider(
            create: (_) => AppBloc(authRepository: _authRepository),
          ),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => CategoryBloc(
              categoryRepository: CategoryRepository(),
            )..add(LoadCategories()),
          ),
          BlocProvider(
            create: (context) => AdsBloc(
              adsRepository: AdsRepository(),
            )..add(LoadAds()),
          ),
          BlocProvider(
            create: (context) =>
                ProductBloc(productRepository: ProductRepository()),
          ),
          BlocProvider(
            create: (_) => WishlistBloc(
                localStorageRepository: LocalStorageRepository(),
                wishListRepository: WishListRepository())
              ..add(StartWishlist()),
          ),
          BlocProvider(
            create: (_) => PreferencesBloc(prefRepository: PrefsRepository())
              ..add(LoadPreferences()),
          ),
          // BlocProvider(
          //   create: (_) => AppBloc(authRepository: _authRepository),
          // ),

          // BlocProvider(
          //   create: (context) => GeolocationBloc(
          //     geolocationRepository: GeolocationRepository(),
          //   ),
          // ),
          // BlocProvider(
          //   create: (context) => SubjectBloc(),
          // ),
        ],
        child: AppView(),
      ),
    );
  }
}

// class AppView extends StatelessWidget {
//   const AppView({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'iGift',
//       theme: ThemeData(
//         primarySwatch: Colors.purple,
//         visualDensity: VisualDensity.adaptivePlatformDensity,
//       ),
//       onGenerateRoute: AppRouter.onGenerateRoute,
//       initialRoute: LoginScreen.routeName,
//       // home: FlowBuilder(
//       //   state: context.select((AppBloc bloc) => bloc.state.status),
//       //   onGeneratePages: onGenerateAppViewPages,
//       // ),
//     );
//   }
// }

class AppView extends StatefulWidget {
  AppView({Key? key}) : super(key: key);

  @override
  State<AppView> createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  @override
  void initState() {
    super.initState();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                // description: channel.description,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
            ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'iGift',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: AppRouter.onGenerateRoute,
      initialRoute: LoginScreen.routeName,
      // home: FlowBuilder(
      //   state: context.select((AppBloc bloc) => bloc.state.status),
      //   onGeneratePages: onGenerateAppViewPages,
      // ),
    );
  }
}
