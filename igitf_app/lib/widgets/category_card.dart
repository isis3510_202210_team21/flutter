import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CategoryWidget extends StatelessWidget {
  String pathImage;
  CategoryWidget({required this.pathImage, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print(pathImage);
      },
      child: Container(
        width: 80.0,
        height: 80.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.grey,
          image: DecorationImage(
            image: CachedNetworkImageProvider(pathImage),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}