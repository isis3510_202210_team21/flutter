import 'package:flutter/material.dart';
import 'package:igift/widgets/category_card.dart';

class ListCategory extends StatelessWidget {
  var images = {
    'airplane.png': 'Airplane',
    'couch.png': 'Couch',
    'dish.png': 'Dish',
    'electronics.png': 'Electronics',
    'tshirt.png': 'Tshirt',
  };

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120.0,
      width: double.maxFinite,
      margin: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: ListView.builder(
          itemCount: 5,
          scrollDirection: Axis.horizontal,
          itemBuilder: (
            _,
            index,
          ) {
            return Container(
              margin: const EdgeInsets.only(right: 20.0),
              child: Column(
                children: [
                  CategoryWidget(
                      pathImage: 'assets/${images.keys.elementAt(index)}'),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    child: Text(images.values.elementAt(index)),
                  ),
                ],
              ),
            );
          }),
    );
  }
}
