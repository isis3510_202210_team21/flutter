import 'package:flutter/material.dart';
import 'package:igift/screens/profile_screen.dart';
import 'package:igift/screens/map_screen.dart';

class CustomNavBar extends StatelessWidget {
  const CustomNavBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Colors.white,
      child: Container(
        height: 70.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
                icon: Icon(
                  Icons.home,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/');
                }),
            IconButton(
                icon: Icon(Icons.map, color: Colors.black),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return GoogleMapPage();
                  }));
                }),
            IconButton(
                icon: Icon(Icons.auto_awesome, color: Colors.black),
                onPressed: () {
                  Navigator.pushNamed(context, '/wishlist');
                }),
            IconButton(
                icon: Icon(Icons.person, color: Colors.black),
                onPressed: () {
                  Navigator.pushNamed(context, '/profile');
                }),
          ],
        ),
      ),
    );
  }
}
