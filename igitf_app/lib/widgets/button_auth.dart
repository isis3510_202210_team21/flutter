import 'package:flutter/material.dart';

class ButtonAuth extends StatelessWidget {
  final Color? textColor;
  final String text;
  double size;
  double height;
  final VoidCallback onPressed;
  ButtonAuth(
      {Key? key,
      this.textColor,
      required this.text,
      this.size = 12,
      this.height = 1.2,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
        width: size.width * 0.5,
        height: 40,
        child: TextButton(
          child: Text(text,
              style: TextStyle(
                  fontSize: 18,
                  color: textColor,
                  fontWeight: FontWeight.bold)),
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.white),
            foregroundColor: MaterialStateProperty.all(textColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(29),
              ),
            ),
          ),
          onPressed: onPressed,
        ));
  }
}
