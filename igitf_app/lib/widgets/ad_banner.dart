import 'package:google_mobile_ads/google_mobile_ads.dart';
import '../ad_helper.dart';
import 'package:flutter/material.dart';

class AdBanner extends State {
  late BannerAd _ad;
  late bool isLoaded = false;

  @override
  void initState() {
    super.initState();
    _ad = BannerAd(
      adUnitId: AdHelper.bannerAdUnitId,
      request: AdRequest(),
      size: AdSize.banner,
      listener: BannerAdListener(
        onAdLoaded: (_) {
          setState(_) {
            isLoaded = true;
          }

          ;
        },
        onAdFailedToLoad: (_, error) {
          print('Ad Failed to Load: $error');
        },
      ),
    );
    _ad.load();
  }

  void dispose() {
    _ad.dispose();
    super.dispose();
  }

  Widget checkForAd() {
    if (isLoaded == true) {
      return Container(
        child: AdWidget(ad: _ad),
        width: _ad.size.width.toDouble(),
        alignment: Alignment.center,
      );
    } else {
      return CircularProgressIndicator();
    }
  }

  @override
  Widget build(BuildContext context) {
    return checkForAd();
  }
}
