import 'package:flutter/material.dart';
import 'package:igift/models/product_model.dart';

class ProducCard extends StatelessWidget {
  final ProductModel product;
  const ProducCard({
    required this.product,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final makeListTile = ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      leading: Container(
        padding: const EdgeInsets.only(right: 12.0),
        decoration: const BoxDecoration(
            border:
                Border(right: BorderSide(width: 1.0, color: Colors.white24))),
        child: Image.network(
          product.imageUrl,
          fit: BoxFit.cover,
          errorBuilder: (context, error, stackTrace) {
            print('Intento ERROR');
            return Icon(Icons.error);
          },
        ),
      ),
      title: Text(
        product.name,
        style:
            const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
      subtitle: Text(
        "${product.currency} \$${product.currentPrice}",
        style: const TextStyle(color: Colors.black),
      ),
      onTap: () {
        Navigator.pushNamed(context, '/product_detail', arguments: product);
      },
    );

    return Card(
      elevation: 8.0,
      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: const BoxDecoration(
          color: Color.fromRGBO(154, 138, 201, 0.5),
        ),
        child: makeListTile,
      ),
    );
  }
}
