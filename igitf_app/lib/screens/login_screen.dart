// ignore_for_file: deprecated_member_use

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:igift/repositories/auth_repository.dart';

import '../cubits/login/login_cubit.dart';
import '../widgets/big_text.dart';
import '../widgets/small_text.dart';
import '../widgets/button_auth.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({
    Key? key,
  }) : super(key: key);

  static Page page() => const MaterialPage<void>(child: LoginScreen());

  static const String routeName = '/login';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => LoginScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Color(0xff6852A5),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(50),
          child: BlocProvider(
            create: (_) => LoginCubit(context.read<AuthRepository>()),
            child: const LoginForm(),
          ),
        ));
  }
}

class LoginForm extends StatelessWidget {
  const LoginForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
        listener: (context, state) {
          if (state.status == LoginStatus.error) {
            //Handler error
          }
          if (state.status == LoginStatus.success) {
            Navigator.pushNamed(context, '/');
          }
        },
        child: Align(
          alignment: const Alignment(0, -1 / 3),
          child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            BlocBuilder<LoginCubit, LoginState>(
              builder: (context, state) {
                if (state.status == LoginStatus.success) {
                  return buildInitialUi();
                } else if (state.status == LoginStatus.error) {
                  return buildFailureUi();
                } else {
                  return buildInitialUi();
                }
              },
            ),
            _EmailInput(),
            const SizedBox(height: 8.0),
            _PasswordInput(),
            const SizedBox(height: 8.0),
            _SignUpButton(),
            const SizedBox(height: 8.0),
            _LoginButton(),
          ],
        ))
        );
  }
}

class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          width: MediaQuery.of(context).size.width * 0.75,
          height: 50,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(29),
          ),
          child: TextField(
            onChanged: (email) =>
                context.read<LoginCubit>().emailChanged(email),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "E-mail",
              hintStyle: TextStyle(
                color: Color(0xFF727171),
              ),
            ),
            keyboardType: TextInputType.emailAddress,
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          width: MediaQuery.of(context).size.width * 0.75,
          height: 50,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(29),
          ),
          child: TextField(
            onChanged: (password) =>
                context.read<LoginCubit>().passwordChanged(password),
            obscureText: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Password",
              hintStyle: TextStyle(
                color: Color(0xFF727171),
              ),
            ),
            keyboardType: TextInputType.visiblePassword,
          ),
        );
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status == LoginStatus.submitting
            ? const CircularProgressIndicator()
            : ButtonAuth(
                text: 'Login',
                onPressed: () {
                  context.read<LoginCubit>().logInWithCredentials();
                });
      },
    );
  }
}

class _SignUpButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      child: FlatButton(
        color: Colors.transparent,
        child: Text("Don’t have a account? Sign-up now!"),
        textColor: Colors.white,
        onPressed: () {
          Navigator.pushNamed(context, '/signup');
        },
      ),
    );
  }
}

Widget buildFailureUi() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      buildInitialUi(),
      Container(
        padding: EdgeInsets.all(5.0),
        child: Text(
          "Please verify your email and password",
          style: TextStyle(color: Colors.white),
        ),
      ),
    ],
  );
}

Widget buildInitialUi() {
  return Container(
    padding: EdgeInsets.all(5.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        BigText(
          text: 'iGift',
          color: Color(0xFFFFFFFF),
          size: 28,
        ),
        SmallText(
          text: 'Login',
          color: Color(0xFFFFFFFF),
          size: 14,
        ),
        Image.asset('assets/login.png',
        width: 350,),
      ],
    ),
  );
}

void navigateToHomeScreen(
  BuildContext context, //User user
) {
  // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
  //   return HomePageParent(user: user, userRepository: userRepository);
  // }));
}
