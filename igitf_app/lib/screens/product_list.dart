import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:igift/bloc/product/product_bloc.dart';
import 'package:igift/widgets/big_text.dart';
import 'package:igift/widgets/custom_appbar.dart';
import 'package:igift/widgets/custom_navbar.dart';
import 'package:igift/widgets/product_card.dart';
import 'package:connectivity/connectivity.dart';
import 'package:igift/widgets/small_text.dart';

class ProductList extends StatefulWidget {
  static const String routeName = '/product_list';

  const ProductList({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      settings: const RouteSettings(name: routeName),
      builder: (_) => ProductList(),
    );
  }

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  late final ScrollController _scrollController;
  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  infinitiveScrollListener(BuildContext context) {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        var a = BlocProvider.of<ProductBloc>(context, listen: false).state
            as ProductLoaded;
        BlocProvider.of<ProductBloc>(context, listen: false)
            .add(LoadMoreProducts(
          products: a.products,
          category: a.category,
        ));
      } else {
        print('NOT WORKS');
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    infinitiveScrollListener(context);
    return Scaffold(
      appBar: const CustomAppBar(title: 'LISTA DE PRODUCTOS'),
      bottomNavigationBar: const CustomNavBar(),
      body: StreamBuilder(
        stream: Connectivity().onConnectivityChanged,
        builder:
            (BuildContext context, AsyncSnapshot<ConnectivityResult> snapshot) {
          if (snapshot.hasData && snapshot.data != ConnectivityResult.none) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: BlocBuilder<ProductBloc, ProductState>(
                builder: ((context, state) {
                  if (state is ProductLoading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state is ProductLoaded) {
                    return ListView.separated(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      separatorBuilder: (context, index) => const Divider(),
                      itemBuilder: (context, index) {
                        return ProducCard(product: state.products[index]);
                      },
                      itemCount: state.products.length,
                      controller: _scrollController,
                    );
                  } else {
                    return const Text('Error');
                  }
                }),
              ),
            );
          } else {
            return (SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                child: Align(
                  alignment: const Alignment(0, -1 / 3),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('assets/no-connection.png'),
                      BigText(
                        text: 'Unable to connect',
                        color: const Color(0xff6852A5),
                        size: 28,
                      ),
                      const SizedBox(height: 8),
                      SmallText(
                        text:
                            'Check the connectivity in your device.\nIf you are connected to the internet, please try again.',
                        color: const Color(0xff6852A5),
                        size: 14,
                      ),
                    ],
                  ),
                ),
              ),
            ));
          }
        },
      ),
    );
  }
}
