import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubits/sign_up/sign_up_cubit.dart';
import '../repositories/auth_repository.dart';

import '../widgets/big_text.dart';
import '../widgets/small_text.dart';
import '../widgets/button_auth.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

    static const String routeName = '/signup';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => SignUpScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xff6852A5),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(50),
        child: BlocProvider<SignUpCubit>(
          create: (_) => SignUpCubit(context.read<AuthRepository>()),
          child: const SignUpForm(),
        ),
      ),
    );
  }
}

class SignUpForm extends StatelessWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignUpCubit, SignUpState>(
      listener: (context, state) {
        if (state.status == SignUpStatus.error) {
            //Handler error
        }
        if (state.status == SignUpStatus.success) {
            Navigator.pushNamed(context, '/');
        }
      },
      child: Align(
        alignment: const Alignment(0, -1 / 3),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            BlocBuilder<SignUpCubit, SignUpState>(
              builder: (context, state) {
                if (state.status == SignUpStatus.success) {
                  return buildInitialUi();
                } else if (state.status == SignUpStatus.error) {
                  return buildFailureUi(state);
                } else {
                  return buildInitialUi();
                }
              },
            ),
            _EmailInput(),
            const SizedBox(height: 8),
            _PasswordInput(),
            const SizedBox(height: 8),
            _LoginButton(),
            const SizedBox(height: 8),
            _SignUpButton(),
          ],
        ),
      ),
    );
  }
}
class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpCubit, SignUpState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          width: MediaQuery.of(context).size.width * 0.75,
          height: 50,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(29),
          ),
          child: TextField(
            onChanged: (email) =>
                context.read<SignUpCubit>().emailChanged(email),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "E-mail",
              hintStyle: TextStyle(
                color: Color(0xFF727171),
              ),
            ),
            keyboardType: TextInputType.emailAddress,
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpCubit, SignUpState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          width: MediaQuery.of(context).size.width * 0.75,
          height: 50,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(29),
          ),
          child: TextField(
            onChanged: (password) =>
                context.read<SignUpCubit>().passwordChanged(password),
            obscureText: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Password",
              hintStyle: TextStyle(
                color: Color(0xFF727171),
              ),
            ),
            keyboardType: TextInputType.visiblePassword,
          ),
        );
      },
    );
  }
}

class _SignUpButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpCubit, SignUpState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status == SignUpStatus.submitting
            ? const CircularProgressIndicator(
              valueColor: const AlwaysStoppedAnimation<Color>(Colors.white),
            )
            : ButtonAuth(
                text: 'Sign up',
                onPressed: () {
                  context.read<SignUpCubit>().signupFormSubmitted();
                });
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      child: FlatButton(
        color: Colors.transparent,
        child: Text("Already have an account? Login!"),
        textColor: Colors.white,
        onPressed: () {
          Navigator.pushNamed(context, '/login');
        },
      ),
    );
  }
}

 Widget buildInitialUi() {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
            BigText(
              text: 'iGift',
              color: Color(0xFFFFFFFF),
              size: 28,
            ),
            SmallText(
              text: 'SignUp',
              color: Color(0xFFFFFFFF),
              size: 14,
            ),
            Image.asset('assets/signup.png',
            width: 350,)
      ],),
    );
  }



  Widget buildFailureUi(SignUpState state) {
    return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      buildInitialUi(),
      Container(
        padding: EdgeInsets.all(5.0),
        child: Text(
          'Check your email or password',
          style: TextStyle(color: Colors.white),
        ),
      ),
    ],
  );
  }

