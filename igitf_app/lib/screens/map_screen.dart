import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:igift/providers/location_provider.dart';
import 'package:igift/widgets/custom_appbar.dart';
import 'package:igift/widgets/custom_navbar.dart';
import 'package:provider/provider.dart';
import 'package:connectivity/connectivity.dart';

import '../widgets/big_text.dart';
import '../widgets/small_text.dart';

class GoogleMapPage extends StatefulWidget {
  GoogleMapPage({Key? key}) : super(key: key);

  @override
  State<GoogleMapPage> createState() => _GoogleMapPageState();
}

class _GoogleMapPageState extends State<GoogleMapPage> {
  @override
  void initState() {
    super.initState();
    if (Connectivity().checkConnectivity() != ConnectivityResult.none) {
      Provider.of<LocationProvider>(context, listen: false).initialization();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'MAP'),
      bottomNavigationBar: CustomNavBar(),
      backgroundColor: Color(0xff6852A5),
      body: Center(
        child: StreamBuilder(
          stream: Connectivity().onConnectivityChanged,
          builder: (BuildContext context,
              AsyncSnapshot<ConnectivityResult> snapshot) {
            if (snapshot.hasData && snapshot.data != ConnectivityResult.none) {
              return googleMapUI();
            } else {
              return (
                SingleChildScrollView(
                  child: Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Align(
                    alignment: const Alignment(0, -1 / 3),
                    child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('assets/no-connection.png'),
                      BigText(
                        text: 'Unable to connect',
                        color: Color(0xFFFFFFFF),
                        size: 28,
                      ),
                      const SizedBox(height: 8),
                      SmallText(
                        text:
                            'Check the connectivity in your device.\nIf you are connected to the internet, please try again.',
                        color: Color(0xFFFFFFFF),
                        size: 14,
                      ),
                    ],
                  ))))
                  );
            }
          },
        ),
      ),
    );
  }

  //4.6014567, -74.0661333

  Widget googleMapUI() {
    return Consumer<LocationProvider>(builder: (consumeContext, model, child) {
      return Column(
        children: [
          Expanded(
              child: GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              target: model.locationPosition,
              zoom: 10,
            ),
            myLocationEnabled: true,
            zoomControlsEnabled: true,
            myLocationButtonEnabled: true,
            onMapCreated: (GoogleMapController controller) {},
          ))
        ],
      );
    });
  }
}
