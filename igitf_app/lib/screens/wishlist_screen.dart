import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:igift/bloc/wishlist/bloc/wishlist_bloc.dart';
import 'package:igift/widgets/custom_appbar.dart';
import 'package:igift/widgets/custom_navbar.dart';
import 'package:igift/widgets/product_card.dart';

class WishListScreen extends StatelessWidget {
  static const String routeName = '/wishlist';
  const WishListScreen({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => WishListScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'Wishlist'),
      bottomNavigationBar: CustomNavBar(),
      body: BlocBuilder<WishlistBloc, WishlistState>(
        builder: (context, state) {
          if (state is WishlistLoading) {
            return Center(
              child: CircularProgressIndicator(
                color: Colors.black,
              ),
            );
          }
          if (state is WishlistLoaded) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                separatorBuilder: (context, index) => const Divider(),
                itemBuilder: (context, index) {
                  return ProducCard(product: state.wishlist.products[index]);
                },
                itemCount: state.wishlist.products.length,
                // controller: _scrollController,
              ),
            );
          }
          return Text('Something went wrong!');
        },
      ),
    );
  }
}
