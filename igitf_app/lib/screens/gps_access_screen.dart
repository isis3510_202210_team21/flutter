import 'package:flutter/material.dart';

class GpsAccessScreen extends StatelessWidget {
  const GpsAccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: _AccessButton(),
      ),
    );
  }
}

class _EnableGPSMessage extends StatelessWidget {
  const _EnableGPSMessage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Debe habilitar el GPS',
      style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300),
    );
  }
}

class _AccessButton extends StatelessWidget {
  const _AccessButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
      'Es necesario el acceso al GPS',
      style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300),
      ),
      MaterialButton(
        child: const Text('Solicitar Acceso', style: TextStyle( color: Colors.white )),
        color: Colors.black,
        shape: const StadiumBorder(),
        onPressed: () {
          //TODO;
        },
      ),
      ],
    );
  }
}
