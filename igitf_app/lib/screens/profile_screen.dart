import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:igift/bloc/preferences/preferences_bloc.dart';
import 'package:igift/repositories/auth_repository.dart';
import 'package:igift/screens/login_screen.dart';
import 'package:connectivity/connectivity.dart';
import 'package:igift/widgets/custom_appbar.dart';
import 'package:igift/widgets/custom_navbar.dart';

import 'login_screen.dart';
import '../bloc/app/app_bloc.dart';
import '../cubits/login/login_cubit.dart';
import '../widgets/big_text.dart';
import '../widgets/small_text.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  static const String routeName = '/profile';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => ProfileScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(title: 'Profile'),
        bottomNavigationBar: CustomNavBar(),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: BlocProvider(
            create: (_) => LoginCubit(context.read<AuthRepository>()),
            child: const ProfileView(),
          ),
        ));
  }
}

class ProfileView extends StatelessWidget {
  const ProfileView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (context, state) {
        if (state.status == LoginStatus.error) {
          //Handler error
        }
      },
      child: StreamBuilder(
        stream: Connectivity().onConnectivityChanged,
        builder:
            (BuildContext context, AsyncSnapshot<ConnectivityResult> snapshot) {
          if (snapshot.hasData && snapshot.data != ConnectivityResult.none) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _LogOutBtn(),
                BlocBuilder<PreferencesBloc, PreferencesState>(
                  builder: (context, state) {
                    if (state is LoadPreferences) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state is PreferencesLoaded) {
                      return Column(
                        children: [
                          Text(
                              'Preferences: ${state.prefs[0].preferences['Shoes']}'),
                          Checkbox(
                            value: state.prefs[0].preferences['Shoes'],
                            onChanged: (bool? value) {
                              print(value);
                              state.prefs[0].preferences['Shoes'] = value;
                            },
                          )
                        ],
                      );
                    } else {
                      return Text('No Carga');
                    }
                  },
                ),
              ],
            );
          } else {
            return (SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                child: Align(
                  alignment: const Alignment(0, -1 / 3),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('assets/no-connection.png'),
                      BigText(
                        text: 'Unable to connect',
                        color: const Color(0xff6852A5),
                        size: 28,
                      ),
                      const SizedBox(height: 8),
                      SmallText(
                        text:
                            'Check the connectivity in your device.\nIf you are connected to the internet, please try again.',
                        color: const Color(0xff6852A5),
                        size: 14,
                      ),
                    ],
                  ),
                ),
              ),
            ));
          }
        },
      ),
    );
  }
}

class _LogOutBtn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return ElevatedButton(
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            primary: Color.fromARGB(255, 149, 6, 3),
          ),
          onPressed: () {
            // Navigator.pushNamed(context, '/login');
            // context.read<AppBloc>().add(AppLogoutRequested());
          },
          child: const Text('LOGOUT'),
        );
      },
    );
  }
}
