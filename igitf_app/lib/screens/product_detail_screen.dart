import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:igift/bloc/wishlist/bloc/wishlist_bloc.dart';
import 'package:igift/models/product_model.dart';
import 'package:igift/widgets/big_text.dart';
import 'package:igift/widgets/custom_navbar.dart';
import 'package:connectivity/connectivity.dart';
import 'package:igift/widgets/small_text.dart';

class ProductDetailScreen extends StatelessWidget {
  static const String routeName = '/product_detail';
  static Route route({required ProductModel product}) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => ProductDetailScreen(product: product),
    );
  }

  final ProductModel product;
  const ProductDetailScreen({required this.product, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: const CustomNavBar(),
      body: Center(
        child: StreamBuilder(
          stream: Connectivity().onConnectivityChanged,
          builder: (BuildContext context,
              AsyncSnapshot<ConnectivityResult> snapshot) {
            if (!snapshot.hasData && snapshot.data != ConnectivityResult.none) {
              return Stack(
                children: [
                  Positioned(
                    top: 0,
                    child: Container(
                      alignment: Alignment.topCenter,
                      height: size.height - 300,
                      width: size.width,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          alignment: Alignment.bottomRight,
                          fit: BoxFit.cover,
                          image: NetworkImage(product.imageUrl),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      height: size.height / 2.7,
                      width: size.width,
                      decoration: const BoxDecoration(
                        color: Color.fromRGBO(154, 138, 201, 1),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20.0),
                          topLeft: Radius.circular(20.0),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Container(
                                margin: const EdgeInsets.only(bottom: 16),
                                height: 5,
                                width: 32 * 1.5,
                                decoration: BoxDecoration(
                                  gradient: const LinearGradient(
                                    begin: Alignment.topRight,
                                    end: Alignment.bottomLeft,
                                    colors: [
                                      Colors.blue,
                                      Colors.red,
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(3),
                                ),
                              ),
                            ),
                            ProductNameAndPrice(
                                name: product.name,
                                price: product.currentPrice,
                                currency: product.currency),
                            const SizedBox(
                              height: 16,
                            ),
                            Text(
                              product.subcategory,
                              // style: AppStyle.text
                              //     .copyWith(color: Colors.white.withOpacity(.8)),
                            ),
                            const Spacing(),
                            const Text(
                              'This is weekdays design-your go-to for all the latest trends, no matter who you are.',
                            ),
                            const Spacing(),
                            Center(
                              child: BlocBuilder<WishlistBloc, WishlistState>(
                                builder: (context, state) {
                                  if (state is WishlistLoading) {
                                    return CircularProgressIndicator();
                                  }
                                  if (state is WishlistLoaded) {
                                    print('Button wishlist loaded');
                                    return IconButton(
                                      icon: const Icon(Icons.favorite,
                                          color: Colors.white),
                                      onPressed: () {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          const SnackBar(
                                            content:
                                                Text('Added to your Wishlist!'),
                                          ),
                                        );
                                        context
                                            .read<WishlistBloc>()
                                            .add(AddProductToWishlist(product));
                                      },
                                    );
                                  }
                                  return const Text('Something went wrong!');
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            } else {
              return (SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Align(
                    alignment: const Alignment(0, -1 / 3),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset('assets/no-connection.png'),
                        BigText(
                          text: 'Unable to connect',
                          color: const Color.fromRGBO(154, 138, 201, 1),
                          size: 28,
                        ),
                        const SizedBox(height: 8),
                        SmallText(
                          text:
                              'Check the connectivity in your device.\nIf you are connected to the internet, please try again.',
                          color: const Color.fromRGBO(154, 138, 201, 1),
                          size: 14,
                        ),
                      ],
                    ),
                  ),
                ),
              ));
            }
          },
        ),
      ),
    );
  }
}

class ProductNameAndPrice extends StatelessWidget {
  final String name;
  final String currency;
  final double price;
  const ProductNameAndPrice(
      {Key? key,
      required this.name,
      required this.price,
      required this.currency})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: 150.0,
          height: 30.0,
          child: Text(
            name,
          ),
        ),
        Text(
          '$currency \$$price',
        ),
      ],
    );
  }
}

class Spacing extends StatelessWidget {
  const Spacing({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 16,
    );
  }
}

class TabTitle extends StatelessWidget {
  final String label;
  final bool selected;
  const TabTitle({
    Key? key,
    required this.label,
    required this.selected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            label,
          ),
          const SizedBox(
            height: 4,
          ),
          if (selected)
            Container(
              width: 21,
              height: 2,
            )
        ])
      ],
    );
  }
}
