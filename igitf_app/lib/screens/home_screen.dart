import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:igift/bloc/category/category_bloc.dart';
import 'package:igift/bloc/product/product_bloc.dart';
import 'package:igift/widgets/custom_appbar.dart';
import 'package:igift/widgets/custom_navbar.dart';
import 'package:igift/bloc/ads/ads_bloc.dart';

class HomeScreen extends StatelessWidget {
  static const String routeName = '/';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => HomeScreen(),
    );
  }

  const HomeScreen({
    Key? key,
  }) : super(key: key);

  static Page page() => const MaterialPage<void>(child: HomeScreen());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'iGift'),
      bottomNavigationBar: CustomNavBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            BlocBuilder<AdsBloc, AdsState>(builder: (context, state) {
              if (state is AdsLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is AdsLoaded) {
                final ads = state.ads;
                return Center(
                  child: GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      childAspectRatio: 1.9,
                    ),
                    itemCount: ads.length,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {
                          BlocProvider.of<AdsBloc>(context, listen: false)
                              .add(AddClickAd(ad: ads[index]));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            color: Colors.transparent,
                            image: DecorationImage(
                              image: NetworkImage(ads[index].imageUrl),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
              } else {
                return const Center(
                  child: Text('Error'),
                );
              }
            }),
            const SizedBox(height: 5.0),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: BlocBuilder<CategoryBloc, CategoryState>(
                builder: (context, state) {
                  if (state is CategoryLoading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state is CategoryLoaded) {
                    final categories = state.categories;
                    return Center(
                      child: GridView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 1.0, vertical: 1.0),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 1.0,
                        ),
                        itemCount: categories.length,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              //Navigator.pushNamed(context, '/profile');
                              BlocProvider.of<ProductBloc>(context,
                                      listen: false)
                                  .add(ChangeCategoryOfProducts(
                                      category: categories[index]
                                          .name
                                          .toLowerCase()));
                              Navigator.pushNamed(context, '/product_list');
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  bottom: 20.0, right: 5.0),
                              child: Stack(
                                children: [
                                  Container(
                                    width: 180.0,
                                    height: 200.0,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: const Color.fromRGBO(
                                          154, 138, 201, 0.5),
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Container(
                                        width: 180.0,
                                        height: 110.0,
                                        decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.only(
                                            topRight: Radius.circular(20.0),
                                            topLeft: Radius.circular(20.0),
                                          ),
                                          image: DecorationImage(
                                            image: NetworkImage(
                                                categories[index].imageUrl),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 15.0),
                                      Container(
                                        // padding: const EdgeInsets.all(50.0),
                                        width: 100.0,
                                        height: 30.0,
                                        color: Colors.transparent,
                                        child: Center(
                                          child: Text(
                                            categories[index].name,
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return const Center(
                      child: Text('Error'),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
