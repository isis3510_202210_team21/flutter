import 'package:igift/models/ads_model.dart';

abstract class BaseAdsRepository {
  Stream<List<AdsModel>> updateClickAd(AdsModel ad);

  Stream<List<AdsModel>> getAllAds();
}