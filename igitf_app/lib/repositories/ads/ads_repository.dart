import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:igift/models/ads_model.dart';
import 'package:igift/repositories/ads/base_ads_repository.dart';

class AdsRepository extends BaseAdsRepository {
  final FirebaseFirestore _firebaseFirestore;

  AdsRepository({
    FirebaseFirestore? firebaseFirestore,
  }) : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;

  @override
  Stream<List<AdsModel>> getAllAds() {
    return _firebaseFirestore.collection('ads').snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return AdsModel.fromSnapshot(doc);
      }).toList();
    });
  }

  @override
  Stream<List<AdsModel>> updateClickAd(AdsModel ad) {

    var a = _firebaseFirestore
        .collection('ads')
        .where('name', isEqualTo: ad.name)
        .limit(1);

    a.get().then((value) {
      String ad_id = value.docs[0].id;
      print('current '+ad_id);
      int actualNumClicks = ad.numClicks + 1;

      _firebaseFirestore
        .collection('ads')
        .doc(ad_id)
        .update({'numClicks': actualNumClicks});

    });
    
    return getAllAds();
  }
}
