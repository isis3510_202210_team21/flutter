import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:igift/models/product_model.dart';
import 'package:igift/repositories/product/base_product_repository.dart';

class ProductRepository extends BaseProductRepository {
  final FirebaseFirestore _firebaseFirestore;
  DocumentSnapshot<Object?>? _snapshot;
  String? _category;

  ProductRepository(
      {FirebaseFirestore? firebaseFirestore,
      DocumentSnapshot<Object?>? snapshot,
      String? category})
      : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;
  @override
  Stream<List<ProductModel>> getProductsByCategory({required String category}) {
    if (_category == null) {
      _category = category;
    } else if (_category != category) {
      _category = category;
      _snapshot = null;
    }
    if (_snapshot == null) {
      var a = _firebaseFirestore
          .collection('productos')
          .where('category', isEqualTo: category)
          .orderBy('likes_count', descending: true)
          .limit(20);
      a.get().then((value) {
        print('DOCS1 ${value.docs.length}');
        print('DOCS1 ${value.docs.last['likes_count']}');
        _snapshot = value.docs.last;
      });
      return a.snapshots().map((snapshot) {
        return snapshot.docs.map((doc) {
          return ProductModel.fromSnapshot(doc);
        }).toList();
      });
    } else {
      var b = _firebaseFirestore
          .collection('productos')
          .where('category', isEqualTo: category)
          .orderBy('likes_count', descending: true)
          .limit(1);
      // b.get().then((value) {
      //   _snapshot = value.docs.last;
      // });
      return b.startAfterDocument(_snapshot!).snapshots().map((snapshot) {
        print('DOCS ${snapshot.docs.length}');
        print('DOCS ${snapshot.docs.last['likes_count']}');
        _snapshot = snapshot.docs.last;
        return snapshot.docs.map((doc) {
          return ProductModel.fromSnapshot(doc);
        }).toList();
      });
    }
  }
}
