import 'package:igift/models/product_model.dart';

abstract class BaseProductRepository {
  Stream<List<ProductModel>> getProductsByCategory({required String category});
}
