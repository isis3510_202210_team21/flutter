import 'package:hive/hive.dart';
import 'package:igift/models/product_model.dart';
import 'package:igift/repositories/local_storage/base_local_storage_repository.dart';

class LocalStorageRepository extends BaseLocalStorageRepository {
  String boxName = 'wishlist_products';
  Type boxType = ProductModel;

  @override
  Future<Box> openBox() async {
    Box box = await Hive.openBox<ProductModel>(boxName);
    return box;
  }

  @override
  List<ProductModel> getWishlist(Box box) {
    return box.values.toList() as List<ProductModel>;
  }

  @override
  Future<void> addProductToWishlist(Box box, ProductModel product) async {
    await box.put(product.name, product);
  }

  @override
  Future<void> removeProductFromWishlist(Box box, ProductModel product) async {
    await box.delete(product.name);
  }

  @override
  Future<void> clearWishlist(Box box) async {
    await box.clear();
  }
}
