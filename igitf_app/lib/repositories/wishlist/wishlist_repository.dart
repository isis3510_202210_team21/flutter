import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:igift/models/wishlist_model.dart';
import 'package:igift/repositories/wishlist/base_wishlist_repository.dart';

class WishListRepository extends BaseWishListRepository {
  final FirebaseFirestore _firebaseFirestore;

  WishListRepository({
    FirebaseFirestore? firebaseFirestore,
  }) : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;

  @override
  Future<void> addWishlist(WishlistModel wishlist) {
    print('addWishlist ${wishlist.toString()}');
    return _firebaseFirestore.collection('wishlist').add(wishlist.toDocument());
  }
}
