import 'package:igift/models/wishlist_model.dart';

abstract class BaseWishListRepository {
  Future<void> addWishlist(WishlistModel wishlist);
}
