import 'package:igift/models/prefs.dart';

abstract class BasePreferencesRepository {
  Stream<List<PrefsUserModel>> getUserPreferences({required String userEmail});
}
