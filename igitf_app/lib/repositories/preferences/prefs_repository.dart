import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:igift/models/prefs.dart';
import 'package:igift/repositories/preferences/base_preferences.dart';

class PrefsRepository extends BasePreferencesRepository {
  final FirebaseFirestore _firebaseFirestore;

  PrefsRepository({
    FirebaseFirestore? firebaseFirestore,
  }) : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;

  @override
  Stream<List<PrefsUserModel>> getUserPreferences({required String userEmail}) {
    return _firebaseFirestore
        .collection('users')
        .where('email', isEqualTo: userEmail)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        print('PREFS: ${doc.data()}');
        return PrefsUserModel.fromSnapshot(doc);
      }).toList();
    });
  }
}
