import 'package:hive/hive.dart';
import 'package:igift/models/prefs.dart';
import 'package:igift/repositories/prefs/base_local_storage_repository.dart';

class PrefsLocalStorageRepository extends BasePrefsRepository {
  String boxName = 'prefs_user';
  Type boxType = PrefsUserModel;

  @override
  Future<Box> openBox() async {
    Box box = await Hive.openBox<PrefsUserModel>(boxName);
    return box;
  }

  @override
  Future<void> addPref(Box box, PrefsUserModel product) async {
    await box.put(product.email, product);
  }

  @override
  Future<void> removePref(Box box, PrefsUserModel product) {
    return box.delete(product.email);
  }

  @override
  Future<void> clearPrefs(Box box) {
    // TODO: implement clearPrefs
    throw UnimplementedError();
  }
}
