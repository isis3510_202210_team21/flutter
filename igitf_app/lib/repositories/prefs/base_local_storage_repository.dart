import 'package:hive/hive.dart';
import 'package:igift/models/prefs.dart';

abstract class BasePrefsRepository {
  Future<Box> openBox();
  // List<PrefsUserModel> getPrefs(Box, String property);
  Future<void> addPref(Box box, PrefsUserModel product);
  Future<void> removePref(Box box, PrefsUserModel product);
  Future<void> clearPrefs(Box box);
}
