import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:location/location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationProvider with ChangeNotifier {
  // late BitmapDescriptor _pinLocationIcon;
  // BitmapDescriptor get pinLocationIcon => _pinLocationIcon;
  // late Map<MarkerId, Marker> _markers;
  // Map<MarkerId, Marker> get markers => _markers;

  late Location _location;
  Location get location => _location;
  late LatLng _locationPosition;
  LatLng get locationPosition => _locationPosition;

  LocationProvider() {
    _location = new Location();
    _locationPosition = new LatLng(4.7110, 74.0721);
  }

  initialization() async {
    await getUserLocation();
    // await setCutomMapPin();
  }

  getUserLocation() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();

      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        print('Permisos denegados');
        return;
      }
    }

    location.onLocationChanged.listen((LocationData currentLocation) {
      _locationPosition =
          LatLng(currentLocation.latitude!, currentLocation.longitude!);
      print(
          '--------------------------- $_locationPosition ------------------------');
      // _markers = <MarkerId, Marker>{};
      // Marker marker = Marker(
      //     markerId: markerId,
      //     position: LatLng(4.58475904, -74.10441165),
      //     icon: pinLocationIcon,
      //     draggable: true,
      //     onDragEnd: ((newPosition) {
      //       print('$newPosition newPosition------------------------');
      //       _locationPosition =
      //           LatLng(newPosition.latitude, newPosition.longitude);

      //       notifyListeners();
      //     }));

      // _markers[markerId] = marker;

      notifyListeners();
    });
  }

  // setCutomMapPin() async {
  //   _pinLocationIcon = await BitmapDescriptor.defaultMarker;
  // }
}
